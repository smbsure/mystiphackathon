package com.smbsure.MyStip.respository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.smbsure.MyStip.Model.HospitalProfile;

@Transactional
@Repository
public interface HospitalRepository extends CrudRepository<HospitalProfile, String> {
	
	@Query("select hospitalid from HospitalProfile where ST_Distance_sphere(ST_GeomFromText(:coordinate), ST_GeomFromText(ST_AsText(geo)))>3000")
	List<Integer> getDetails(@Param("coordinate") String coordinate);

	HospitalProfile findByHospitalid(int hospitalid);
}
