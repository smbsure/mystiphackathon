package com.smbsure.MyStip.respository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.smbsure.MyStip.Model.UserProfile;

@Transactional
@Repository
public interface UserRepository extends CrudRepository<UserProfile, String> {
	
	List<UserProfile> findByUsername(String username);

	UserProfile findByUserid(Integer user_id);
	
	UserProfile findByDevicesimno(String devicesimno);
}
