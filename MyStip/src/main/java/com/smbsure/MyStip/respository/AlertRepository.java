package com.smbsure.MyStip.respository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.smbsure.MyStip.Model.Alerts;

@Transactional
@Repository
public interface AlertRepository extends CrudRepository<Alerts, String> {
	
	Alerts findByAlertid(int alertid);
	
	@Modifying
	@Query("Update Alerts set hospitalresponed=:hospitalid where alertid=:alertid")
	int updateHospitalResponse(@Param("hospitalid") int hospitalid,@Param("alertid") int alertid);

}
