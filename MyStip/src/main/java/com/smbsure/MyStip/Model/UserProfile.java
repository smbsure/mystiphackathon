package com.smbsure.MyStip.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.mysql.jdbc.Blob;

@Entity
@Table(name = "User_Profile")
public class UserProfile {
	@Id
	@GeneratedValue
	@Column(name = "userid")
	int userid;

	@Column(name = "username")
	String username;

	@Column(name = "photo")
	String photo;

	@Column(name = "age")
	int age;

	@Column(name = "gender")
	String gender;

	@Column(name = "emergency1")
	String emergency1;

	@Column(name = "emergency2")
	String emergency2;

	@Column(name = "emergency3")
	String emergency3;

	@Column(name = "emergency1relation")
	String emergency1relation;

	@Column(name = "emergency2relation")
	String emergency2relation;

	@Column(name = "emergency3relation")
	String emergency3relation;

	@Column(name = "emergency1relationname")
	String emergency1relationname;

	@Column(name = "emergency2relationname")
	String emergency2relationname;

	@Column(name = "emergency3relationname")
	String emergency3relationname;

	@Column(name = "bloodgroup")
	String bloodgroup;

	@Column(name = "imedeviceid")
	String imedeviceid;

	@Column(name = "devicesimno")
	String devicesimno;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmergency1() {
		return emergency1;
	}

	public void setEmergency1(String emergency1) {
		this.emergency1 = emergency1;
	}

	public String getEmergency2() {
		return emergency2;
	}

	public void setEmergency2(String emergency2) {
		this.emergency2 = emergency2;
	}

	public String getEmergency3() {
		return emergency3;
	}

	public void setEmergency3(String emergency3) {
		this.emergency3 = emergency3;
	}

	public String getEmergency1relation() {
		return emergency1relation;
	}

	public void setEmergency1relation(String emergency1relation) {
		this.emergency1relation = emergency1relation;
	}

	public String getEmergency2relation() {
		return emergency2relation;
	}

	public void setEmergency2relation(String emergency2relation) {
		this.emergency2relation = emergency2relation;
	}

	public String getEmergency3relation() {
		return emergency3relation;
	}

	public void setEmergency3relation(String emergency3relation) {
		this.emergency3relation = emergency3relation;
	}

	public String getEmergency1relationname() {
		return emergency1relationname;
	}

	public void setEmergency1relationname(String emergency1relationname) {
		this.emergency1relationname = emergency1relationname;
	}

	public String getEmergency2relationname() {
		return emergency2relationname;
	}

	public void setEmergency2relationname(String emergency2relationname) {
		this.emergency2relationname = emergency2relationname;
	}

	public String getEmergency3relationname() {
		return emergency3relationname;
	}

	public void setEmergency3relationname(String emergency3relationname) {
		this.emergency3relationname = emergency3relationname;
	}

	public String getBloodgroup() {
		return bloodgroup;
	}

	public void setBloodgroup(String bloodgroup) {
		this.bloodgroup = bloodgroup;
	}

	public String getImedeviceid() {
		return imedeviceid;
	}

	public void setImedeviceid(String imedeviceid) {
		this.imedeviceid = imedeviceid;
	}

	public String getDevicesimno() {
		return devicesimno;
	}

	public void setDevicesimno(String devicesimno) {
		this.devicesimno = devicesimno;
	}

}
