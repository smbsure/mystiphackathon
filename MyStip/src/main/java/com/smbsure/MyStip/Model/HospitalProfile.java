package com.smbsure.MyStip.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hospital_profile")
public class HospitalProfile {
	@Id
	@GeneratedValue
	@Column(name = "hospitalid")
	int hospitalid;

	@Column(name = "hospitalname")
	String hospitalname;

	@Column(name = "address")
	String address;

	@Column(name = "geo")
	String geo;

	@Column(name = "lat")
	double lat;

	@Column(name = "lag")
	double lag;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLag() {
		return lag;
	}

	public void setLag(double lag) {
		this.lag = lag;
	}

	@Column(name = "emergencyno")
	String emergencyno;

	@Column(name = "ambulancestatus")
	Boolean ambulancestatus;

	@Column(name = "ambulancecount")
	int ambulancecount;

	public int getHospitalid() {
		return hospitalid;
	}

	public void setHospitalid(int hospitalid) {
		this.hospitalid = hospitalid;
	}

	public String getHospitalname() {
		return hospitalname;
	}

	public void setHospitalname(String hospitalname) {
		this.hospitalname = hospitalname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGeo() {
		return geo;
	}

	public void setGeo(String geo) {
		this.geo = geo;
	}

	public String getEmergencyno() {
		return emergencyno;
	}

	public void setEmergencyno(String emergencyno) {
		this.emergencyno = emergencyno;
	}

	public Boolean getAmbulancestatus() {
		return ambulancestatus;
	}

	public void setAmbulancestatus(Boolean ambulancestatus) {
		this.ambulancestatus = ambulancestatus;
	}

	public int getAmbulancecount() {
		return ambulancecount;
	}

	public void setAmbulancecount(int ambulancecount) {
		this.ambulancecount = ambulancecount;
	}

}
