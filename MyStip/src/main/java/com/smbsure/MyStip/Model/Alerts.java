package com.smbsure.MyStip.Model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "alerts")
public class Alerts {

	@Id
	@GeneratedValue
	@Column(name = "alertid")
	int alertid;

	@Column(name = "userid")
	int userid;

	@Column(name = "lat")
	double lat;

	@Column(name = "lag")
	double lag;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLag() {
		return lag;
	}

	public void setLag(double lag) {
		this.lag = lag;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Column(name = "createtimestamp")
	Timestamp createtimestamp;

	@Column(name = "hospitalid1")
	int hospitalid1;

	@Column(name = "hospitalid2")
	int hospitalid2;

	@Column(name = "hospitalid3")
	int hospitalid3;

	@Column(name = "hospitalresponed")
	int hospitalresponed;

	public int getAlertid() {
		return alertid;
	}

	public void setAlertid(int alertid) {
		this.alertid = alertid;
	}

	public Timestamp getCreatetimestamp() {
		return createtimestamp;
	}

	public void setCreatetimestamp(Timestamp createtimestamp) {
		this.createtimestamp = createtimestamp;
	}

	public int getHospitalid1() {
		return hospitalid1;
	}

	public void setHospitalid1(int hospitalid1) {
		this.hospitalid1 = hospitalid1;
	}

	public int getHospitalid2() {
		return hospitalid2;
	}

	public void setHospitalid2(int hospitalid2) {
		this.hospitalid2 = hospitalid2;
	}

	public int getHospitalid3() {
		return hospitalid3;
	}

	public void setHospitalid3(int hospitalid3) {
		this.hospitalid3 = hospitalid3;
	}

	public int getHospitalresponed() {
		return hospitalresponed;
	}

	public void setHospitalresponed(int hospitalresponed) {
		this.hospitalresponed = hospitalresponed;
	}

}
