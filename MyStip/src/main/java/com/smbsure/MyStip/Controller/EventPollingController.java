package com.smbsure.MyStip.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smbsure.MyStip.Model.Alerts;
import com.smbsure.MyStip.Model.HospitalProfile;
import com.smbsure.MyStip.Model.ReceiveMessage;
import com.smbsure.MyStip.Model.UserProfile;
import com.smbsure.MyStip.respository.AlertRepository;
import com.smbsure.MyStip.respository.HospitalRepository;
import com.smbsure.MyStip.respository.UserRepository;


@Controller
@CrossOrigin(origins = "*")
public class EventPollingController {

	@Autowired
	private UserRepository userDao;

	@Autowired
	private AlertRepository alertDao;

	@Autowired
	private HospitalRepository hospitalDao;

	@RequestMapping(value = "/get/user/details", method = RequestMethod.GET)
	@ResponseBody
	public UserProfile getUserDetails(@RequestParam(value = "userid") Integer userid) throws IOException {
		System.out.println("GET USER DETAILS");
		System.out.println("userid:" + userid);
		UserProfile user = userDao.findByUserid(userid);
		System.out.println("Username:" + user.getUsername());
		return user;
	}

	@RequestMapping(value = "/get/user/alert", method = RequestMethod.POST)
	@ResponseBody
	public void getUserAlert(HttpServletRequest request) {
		ReceiveMessage receive = new ReceiveMessage();
		receive.setContent(request.getParameter("content"));
		receive.setComments(request.getParameter("comments"));
		receive.setInNumber(request.getParameter("inNumber"));
		receive.setSender(request.getParameter("sender"));
		System.out.println("Request Param Sender:" + receive.getSender());
		System.out.println("request Param InNumber:" + receive.getInNumber());
		System.out.println("Request Param content:" + request.getParameter("content"));
		String content = receive.getContent();
		double lat = Double.parseDouble(content.substring(23, 32));
		double log = Double.parseDouble(content.substring(38, 47));
		String coordinate = "POINT(" + lat + " " + log + ")";
		// 4
		/*
		 * HashMap<Integer,Double> map=new HashMap<Integer,Double>();
		 * Iterable<HospitalProfile> hospitalIterable= hospitalDao.findAll();
		 * Iterator<HospitalProfile> hospitalIterator
		 * =hospitalIterable.iterator(); while(hospitalIterator.hasNext()){
		 * HospitalProfile hospital =hospitalIterator.next(); GeoApiContext sc =
		 * new
		 * GeoApiContext.Builder().disableRetries().queryRateLimit(10).apiKey(
		 * "AIzaSyCsuzN4_e60pvsIEfcrhX4hVvSsDdrMR0c").build(); DirectionsResult
		 * result = DirectionsApi.getDirections(sc, ""+lat+","+log+"",
		 * ""+hospital.getLat()+","+hospital.getLag()+"").await();
		 * map.put(hospital.getHospitalid(),(double)result.routes[0].legs[0].
		 * distance.inMeters );
		 * System.out.println("Distance:"+result.routes[0].legs[0].distance.
		 * inMeters); } for (Map.Entry<Integer,Double> entry : map.entrySet()) {
		 * System.out.println(entry.getKey() + "/" + entry.getValue()); }
		 * LinkedHashMap<Integer,Double> hashmap=sortHashMapByValues(map);
		 * System.out.println("After Sorting"); for (Map.Entry<Integer,Double>
		 * entry : hashmap.entrySet()) { System.out.println(entry.getKey() + "/"
		 * + entry.getValue()); } Set setkeys = hashmap.keySet(); Iterator
		 * it=setkeys.iterator(); UserProfile userprofile =
		 * userDao.findByDevicesimno(receive.getSender()); Alerts alert = new
		 * Alerts(); alert.setCreatetimestamp(new
		 * Timestamp(System.currentTimeMillis()));
		 * alert.setUserid(userprofile.getUserid()); alert.setLat(lat);
		 * alert.setLag(log); alert.setHospitalid1((Integer) it.next());
		 * alert.setHospitalid2((Integer) it.next());
		 * alert.setHospitalid3((Integer) it.next());
		 * alert.setHospitalresponed(0); alertDao.save(alert); String message =
		 * userprofile.getUsername() +
		 * " near your hospital in EMERGENCY, Please CLICK if you can HELP! http://35.188.60.32/get/user/alert/details?alertid="
		 * + alert.getAlertid(); int count=0; for (Map.Entry<Integer,Double>
		 * entry : hashmap.entrySet()) { count++; HospitalProfile
		 * hospitalprofile = hospitalDao.findByHospitalid((Integer)
		 * entry.getKey()); sendSms(message, hospitalprofile.getEmergencyno());
		 * System.out.println("Send SMS Successfully for number "
		 * +hospitalprofile.getEmergencyno()); if(count==3) break; }
		 */

		// 4
		UserProfile userprofile = userDao.findByDevicesimno(receive.getSender());
		List list = hospitalDao.getDetails(coordinate);
		System.out.println("List 0:" + list.get(0));
		System.out.println("List 1:" + list.get(1));
		System.out.println("List 2:" + list.get(2));
		Alerts alert = new Alerts();
		alert.setCreatetimestamp(new Timestamp(System.currentTimeMillis()));
		alert.setUserid(userprofile.getUserid());
		alert.setLat(lat);
		alert.setLag(log);
		alert.setHospitalid1((Integer) list.get(0));
		alert.setHospitalid2((Integer) list.get(1));
		alert.setHospitalid3((Integer) list.get(2));
		alert.setHospitalresponed(0);
		alertDao.save(alert);
		String message = userprofile.getUsername()
				+ " near your hospital in EMERGENCY, Please CLICK if you can HELP! http://35.188.60.32/get/user/alert/details?alertid="
				+ alert.getAlertid();
		for (int i = 0; i < list.size(); i++) {
			HospitalProfile hospitalprofile = hospitalDao.findByHospitalid((Integer) list.get(i));
			sendSms(message, hospitalprofile.getEmergencyno());
			System.out.println("Send SMS Successfully for number " + hospitalprofile.getEmergencyno());
		}
		System.out.println("list count:" + list.size());
		// System.out.println("SMS Successful!!!");
	}

	@RequestMapping(value = "/get/user/alert/details", method = RequestMethod.GET)
	@ResponseBody
	public UserProfile getUserAlertDetails(@RequestParam(value = "alertid") Integer alertid,
			@RequestParam(value = "hospitalid") Integer hospitalid) {
		Alerts alert = alertDao.findByAlertid(alertid);
		HospitalProfile hospitalprofile = hospitalDao.findByHospitalid(hospitalid);
		UserProfile user = userDao.findByUserid(alert.getUserid());
		if (alert.getHospitalresponed() == 0) {
			alertDao.updateHospitalResponse(hospitalid, alertid);
			return user;
		}
		sendSms("ALREADY a Hospital has responded , Thank you", hospitalprofile.getEmergencyno());
		return null;
	}

	public String sendSms(String messageSMS, String senderNo) {
		try {
			// Construct data
			String apiKey = "apikey=" + URLEncoder.encode("lFccMnVYQX8-pB8abW2wB8In4rSIDwPWUriSmFkRkz", "UTF-8");
			String message = "&message=" + URLEncoder.encode(messageSMS, "UTF-8");
			String sender = "&sender=" + URLEncoder.encode("TXTLCL", "UTF-8");
			String numbers = "&numbers=" + URLEncoder.encode(senderNo, "UTF-8");

			// Send data
			String data = "https://api.textlocal.in/send/?" + apiKey + numbers + message + sender;
			URL url = new URL(data);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);

			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			String sResult = "";
			while ((line = rd.readLine()) != null) {
				// Process line...
				sResult = sResult + line + " ";
			}
			rd.close();
			System.out.println("TRUE!!!");
			return sResult;
		} catch (Exception e) {
			System.out.println("Error SMS " + e);
			return "Error " + e;
		}
	}

	public LinkedHashMap<Integer, Double> sortHashMapByValues(HashMap<Integer, Double> passedMap) {
		List<Integer> mapKeys = new ArrayList<Integer>(passedMap.keySet());
		List<Double> mapValues = new ArrayList<Double>(passedMap.values());
		Collections.sort(mapValues);
		Collections.sort(mapKeys);

		LinkedHashMap<Integer, Double> sortedMap = new LinkedHashMap<Integer, Double>();

		Iterator<Double> valueIt = mapValues.iterator();
		while (valueIt.hasNext()) {
			Double val = valueIt.next();
			Iterator<Integer> keyIt = mapKeys.iterator();

			while (keyIt.hasNext()) {
				Integer key = keyIt.next();
				Double comp1 = passedMap.get(key);
				Double comp2 = (Double) val;

				if (comp1.equals(comp2)) {
					keyIt.remove();
					sortedMap.put(key, val);
					break;
				}
			}
		}
		return sortedMap;
	}
}
