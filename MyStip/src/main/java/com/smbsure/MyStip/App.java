package com.smbsure.MyStip;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App {
	public static void main(String[] args) {
		System.out.println("Hello World!");
		SpringApplication.run(App.class, args);
		/*
		 * GeoApiContext sc = new
		 * GeoApiContext.Builder().disableRetries().queryRateLimit(10).apiKey(
		 * "AIzaSyCsuzN4_e60pvsIEfcrhX4hVvSsDdrMR0c").build(); DirectionsResult
		 * result = DirectionsApi.getDirections(sc, "12.948061,80.194832",
		 * "12.963071,80.245749").await(); double
		 * distance=result.routes[0].legs[0].distance.inMeters;
		 * System.out.println("Distance:"+distance);
		 */
	}
}
